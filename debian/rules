#!/usr/bin/make -f

ROUNDCUBE_PLUGIN_NAME = contextmenu
export ROUNDCUBE_PLUGIN_NAME

%:
	dh $@ --with phpcomposer

GENERATED_JSFILES =
GENERATED_CSSFILES = $(patsubst %.less,%.css,$(wildcard skins/*/*.less))
SRC_FILES := $(filter-out $(GENERATED_JSFILES) $(GENERATED_CSSFILES),$(shell find -P . \( -path ./debian -o -path ./.git \) -prune -o -type f -print))

GENERATED_FILES := $(GENERATED_JSFILES) $(GENERATED_CSSFILES)
JS_MINFILES = $(patsubst %.js,%.min.js,$(filter-out %.min.js,$(filter %.js,$(SRC_FILES) $(GENERATED_FILES))))
CSS_MINFILES = $(patsubst %.css,%.min.css,$(filter-out %.min.css,$(filter %.css,$(SRC_FILES) $(GENERATED_FILES))))
GENERATED_FILES += $(JS_MINFILES) $(CSS_MINFILES)

$(JS_MINFILES): %.min.js: %.js
	uglifyjs --compress --mangle --source-map "base='$(@D)',url='$(@F).map'" \
		-o $@ -- $<

$(CSS_MINFILES): %.min.css: %.css
	cd $(@D) && cleancss --source-map -o $(@F) -- $(<F)

# Generate CSS from LESS sources
# We run the command in the Roundcube tree where can import
# ../../../../skins/elastic/styles/{variables,mixins}.
$(GENERATED_CSSFILES): skins/elastic/%.css: skins/elastic/%.less
	cd /usr/share/roundcube/plugins/jqueryui/themes/elastic && \
		lessc --source-map --rewrite-urls $(CURDIR)/$< $(CURDIR)/$@

GENERATED_MAPFILES = $(addsuffix .map,$(filter %.css %.js,$(GENERATED_FILES)))
GENERATED_FILES += $(GENERATED_MAPFILES)
$(GENERATED_MAPFILES): %.map: | % ;

# Pre-compress minified files
COMPRESSED_FILES = $(JS_MINFILES) $(CSS_MINFILES)
COMPRESSED_FILES := $(addsuffix .gz,$(COMPRESSED_FILES))
GENERATED_FILES += $(COMPRESSED_FILES)
$(COMPRESSED_FILES): %.gz: %
	pigz -11 -mnk -- $<

execute_after_dh_auto_clean:
	@rm -f -- $(GENERATED_FILES)

override_dh_auto_build: $(GENERATED_FILES) ;
.SECONDARY:

execute_after_dh_fixperms:
	find debian/roundcube-plugin-*/usr/share/roundcube/plugins \
		\! -name "*.sh" -type f -execdir chmod -x -- {} +
